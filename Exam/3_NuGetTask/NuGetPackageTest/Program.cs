﻿using MyNuGetLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NuGetPackageTest
{
    class Program
    {
        static void Main(string[] args)
        {
            MyNuGetClass myClass = new MyNuGetClass();
            myClass.SayHello();

            Console.WriteLine("Press any key...");
            Console.ReadKey();
        }
    }
}
