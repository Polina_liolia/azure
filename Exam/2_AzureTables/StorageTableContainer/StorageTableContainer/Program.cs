﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.Azure;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Table;
using Microsoft.WindowsAzure.Storage.Auth;

namespace StorageTableContainer
{
    class Program
    {
        static void Main(string[] args)
        {
            // Parse the connection string and return a reference to the storage account.
            CloudStorageAccount storageAccount = CloudStorageAccount.Parse(
                           CloudConfigurationManager.GetSetting("StorageConnectionString"));
            // Create the table footware.
            CloudTableClient tableFootware = storageAccount.CreateCloudTableClient();
            // Retrieve a reference to the table.
            CloudTable table = tableFootware.GetTableReference("footwear");
            // Create the table if it doesn't exist.
            table.CreateIfNotExists();

            #region Insert entity

            Footwear footware = new Footwear("Nike", "Air005");
            footware.Name = "Super cool";
            footware.Gender = "female";
            footware.Size = 38;

            TableOperation insertOperation = TableOperation.Insert(footware);
            table.Execute(insertOperation);

            #endregion

            #region retrieve and update

            // Create a retrieve operation that takes a customer entity.
            TableOperation retrieveOperation = TableOperation.Retrieve<Footwear>("Nike", "Air005");
            // Execute the operation.
            TableResult retrievedResult = table.Execute(retrieveOperation);
            // Assign the result to a Footwear object.
            Footwear updateEntity = (Footwear)retrievedResult.Result;

            if (updateEntity != null)
            {
                // Change the phone number.
                updateEntity.Name = "not so cool";
                updateEntity.Gender = "male";
                updateEntity.Size = 44;


                // Create the InsertOrReplace TableOperation.
                TableOperation insertOrReplaceOperation = TableOperation.InsertOrReplace(updateEntity);

                // Execute the operation.
                table.Execute(insertOrReplaceOperation);

                Console.WriteLine("Entity was updated.");
            }

            else
                Console.WriteLine("Entity could not be retrieved.");

            #endregion

            #region select size only

            // Define the query, and select only the Email property.
            TableQuery<DynamicTableEntity> projectionQuery = new TableQuery<DynamicTableEntity>().Select(new string[] { "Size" });
            // Define an entity resolver to work with the entity after retrieval.
            EntityResolver<string> resolver = (pk, rk, ts, props, etag) => props.ContainsKey("Size") ? props["Size"].StringValue : null;
            foreach (string projectedSize in table.ExecuteQuery(projectionQuery, resolver, null, null))
            {
                Console.WriteLine(projectedSize);
            }

            #endregion

            #region Delete table

            // Create the CloudTable that represents the "footware" table.
            table = tableFootware.GetTableReference("footwear");
            // Delete the table it if exists.
            table.DeleteIfExists();

            #endregion


            Console.WriteLine("Press any key...");
            Console.ReadKey();
        }


    }


    public class Footwear : TableEntity
    {
        public Footwear(string brand, string model)
        {
            this.PartitionKey = brand;
            this.RowKey = model;
        }

        public double Size { get; set; }
        public string Brand { get; set; }
        public string Name { get; set; }
        public string Gender { get; set; }
        public int Model { get; set; }
    }



}

