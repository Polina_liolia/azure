﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telegram.Bot.Types.ReplyMarkups;

namespace MyBot
{
    public class StateMashine
    {
        private static UserMessage userMessage;
        private StateMashine()
        { }
        
        public static StateMashine Create()
        {
            return new StateMashine();
        }

        private static BotEvents generateEvent(string msgText)
        {
            switch(msgText)
            {
                case "/start": return BotEvents.Start;
                case "/stone": return BotEvents.Stone;
                case "/scissors": return BotEvents.Scissors;
                case "/paper": return BotEvents.Paper;
                case "btnYes": return BotEvents.StartAccepted;
                case "btnNo": return BotEvents.StartRejected;
                default: return BotEvents.Undefined;
            }
        }

        public static async void DefineState(Telegram.Bot.Types.Message msg, string callbackQueryData = "")
        {
            if (userMessage is null)
                userMessage = new UserMessage(msg);
            else
                userMessage.Msg = msg;
            string msgArg = (callbackQueryData == string.Empty) ? msg.Text : callbackQueryData;
            BotEvents currentEvent = generateEvent(msgArg);
            IReplyMarkup replyMarkup = null;
            string reply = userMessage.FindOut(currentEvent, out replyMarkup);
            await TelegramBotSingleton.Bot.SendTextMessageAsync(msg.Chat.Id, reply, replyMarkup: replyMarkup);
        }


    }
}
