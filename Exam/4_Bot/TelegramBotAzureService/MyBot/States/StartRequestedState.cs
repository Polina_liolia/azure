﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telegram.Bot.Types.ReplyMarkups;

namespace MyBot.States
{
    public class StartRequestedState : State
    {
        protected override string ChangeState(UserMessage message, BotEvents botEvent, out IReplyMarkup replyMarkup)
        {
            string answer = "";
            replyMarkup = null;
            switch(botEvent)
            {
                case BotEvents.StartAccepted:
                    answer = "Поехали! Сделайте свой выбор: /stone, /scissors, /paper";
                    message.State = StatesHolderSingleton.Instance["started"];
                    break;
                case BotEvents.StartRejected:
                    answer = "Поиграем в другой раз...";
                    message.State = StatesHolderSingleton.Instance["rejected"];
                    break;
                default:
                    answer = "Вы еще не приняли решение, играть или нет. Нажмите на соответствующую кнопку.";
                    break;
            }
            return answer;
        }
    }
}
