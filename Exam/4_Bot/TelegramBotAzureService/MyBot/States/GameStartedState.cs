﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telegram.Bot.Types.ReplyMarkups;

namespace MyBot.States
{
    public class GameStartedState : State
    {
        protected override string ChangeState(UserMessage message, BotEvents botEvent, out IReplyMarkup replyMarkup)
        {
            string answer = "";
            message.State = StatesHolderSingleton.Instance["started"];
            replyMarkup = new Telegram.Bot.Types.ReplyMarkups.InlineKeyboardMarkup(
                                       new Telegram.Bot.Types.InlineKeyboardButtons.InlineKeyboardButton[][]
                                       {
                                           // First row
                                           new [] {
                                                    // First column
                                                    new Telegram.Bot.Types.InlineKeyboardButtons.InlineKeyboardCallbackButton("Да","btnYes"),
                                                    // Second column
                                                    new Telegram.Bot.Types.InlineKeyboardButtons.InlineKeyboardCallbackButton("Нет","btnNo"),
                                                   },
                                       });
            message.State = StatesHolderSingleton.Instance["startRequested"]; 
            switch (botEvent)
            {
                case BotEvents.Scissors:
                    answer= "А у меня камень - вы проиграли! Сыграем еще?";
                    break;
                case BotEvents.Stone:
                    answer = "А у меня бумага - вы проиграли! Сыграем еще?";
                    break;
                case BotEvents.Paper:
                    answer = "А у меня ножницы - вы проиграли! Сыграем еще?";
                    break;
                default:
                    answer = "Сделайте свой выбор: /stone, /scissors, /paper";
                    replyMarkup = null;
                    message.State = StatesHolderSingleton.Instance["started"];//as GameStartedState;
                    break;
            }
            return answer;
        }
    }
}
