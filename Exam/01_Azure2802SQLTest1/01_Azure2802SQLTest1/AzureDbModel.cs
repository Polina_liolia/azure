namespace _01_Azure2802SQLTest1
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class AzureDbModel : DbContext
    {
        public AzureDbModel()
            : base("name=AzureDbModel")
        {
        }

        public virtual DbSet<Client> Clients { get; set; }
        public virtual DbSet<Order> Orders { get; set; }
        public virtual DbSet<database_firewall_rules> database_firewall_rules { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Client>()
                .Property(e => e.Clients_Name)
                .IsUnicode(false);

            modelBuilder.Entity<Client>()
                .Property(e => e.Clients_Phone)
                .IsFixedLength();

            modelBuilder.Entity<Client>()
                .Property(e => e.Clients_Mail)
                .IsUnicode(false);

            modelBuilder.Entity<Order>()
                .Property(e => e.Description)
                .IsUnicode(false);

            modelBuilder.Entity<database_firewall_rules>()
                .Property(e => e.start_ip_address)
                .IsUnicode(false);

            modelBuilder.Entity<database_firewall_rules>()
                .Property(e => e.end_ip_address)
                .IsUnicode(false);
        }
    }
}
