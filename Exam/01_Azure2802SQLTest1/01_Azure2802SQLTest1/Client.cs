namespace _01_Azure2802SQLTest1
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Client
    {
        [Key]
        [Column(Order = 0)]
        public int Clients_Id { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(30)]
        public string Clients_Name { get; set; }

        [StringLength(20)]
        public string Clients_Phone { get; set; }

        [Key]
        [Column(Order = 2)]
        [StringLength(50)]
        public string Clients_Mail { get; set; }
    }
}
