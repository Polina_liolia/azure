﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _01_Azure2802SQLTest1
{
    class Program
    {
        static void Main(string[] args)
        {
            using (AzureDbModel db = new AzureDbModel())
            {
                db.Clients.Load();
                db.Orders.Load();
                Console.WriteLine("Clients:");
                foreach(Client c in db.Clients)
                {
                    Console.WriteLine($"{c.Clients_Name} {c.Clients_Mail} {c.Clients_Phone}");
                }

                Console.WriteLine("Orders:");
                foreach (Order o in db.Orders)
                {
                    Console.WriteLine($"{o.OrdersDate} {o.TotalCost} UAH  Description: {o.Description}");
                }
            }
            Console.WriteLine("Press any key...");
            Console.ReadKey();
        }
    }
}
