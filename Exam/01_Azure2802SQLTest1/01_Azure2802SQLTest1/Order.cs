namespace _01_Azure2802SQLTest1
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Order
    {
        [Key]
        [Column(Order = 0)]
        public int OrdersId { get; set; }

        [StringLength(150)]
        public string Description { get; set; }

        [Key]
        [Column(Order = 1, TypeName = "date")]
        public DateTime OrdersDate { get; set; }

        [Key]
        [Column(Order = 2)]
        public double TotalCost { get; set; }

        [Key]
        [Column(Order = 3)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int MyClientsId { get; set; }
    }
}
