﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telegram.Bot.Types.ReplyMarkups;

namespace MyBot.States
{
    public class GameRejectedState : State
    {
        protected override string ChangeState(UserMessage message, BotEvents botEvent, out IReplyMarkup replyMarkup)
        {
            string answer = "Вы передумали и хотите играть?";
            replyMarkup = new Telegram.Bot.Types.ReplyMarkups.InlineKeyboardMarkup(
                  new Telegram.Bot.Types.InlineKeyboardButtons.InlineKeyboardButton[][]
                  {
                      // First row
                      new [] {
                               // First column
                               new Telegram.Bot.Types.InlineKeyboardButtons.InlineKeyboardCallbackButton("Да","btnYes"),
                               // Second column
                               new Telegram.Bot.Types.InlineKeyboardButtons.InlineKeyboardCallbackButton("Нет","btnNo"),
                              },
                  });
            message.State = StatesHolderSingleton.Instance["startRequested"];
            return answer;
        }
    }
}
