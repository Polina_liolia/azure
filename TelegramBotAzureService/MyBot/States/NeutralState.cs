﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telegram.Bot;
using Telegram.Bot.Args;
using Telegram.Bot.Types.Enums;
using Telegram.Bot.Types.ReplyMarkups;

namespace MyBot.States
{
    public class NeutralState : State
    {
        protected override string ChangeState(UserMessage message, BotEvents botEvent, out IReplyMarkup replyMarkup)
        {
            StringBuilder sbAnswer = new StringBuilder();
            sbAnswer.AppendLine($"Здравствуйте, я {TelegramBotSingleton.BotName}!");
            replyMarkup = null;
            switch (botEvent)
            {
                case BotEvents.Start:
                    {
                        string firstName = message.Msg.From.FirstName;
                        string lastName = message.Msg.From.LastName;
                        string userName = message.Msg.From.Username;
                        sbAnswer.AppendLine($"{firstName} {lastName}, сыграем в игру 'Камень-ножницы-бумага'?");
                        sbAnswer.AppendLine($"/stone - камень");
                        sbAnswer.AppendLine($"/scissors - ножницы");
                        sbAnswer.AppendLine($"/paper - бумага");
                        message.State = StatesHolderSingleton.Instance["startRequested"];
                        replyMarkup = new Telegram.Bot.Types.ReplyMarkups.InlineKeyboardMarkup(
                                       new Telegram.Bot.Types.InlineKeyboardButtons.InlineKeyboardButton[][]
                                       {
                                           // First row
                                           new [] {
                                                    // First column
                                                    new Telegram.Bot.Types.InlineKeyboardButtons.InlineKeyboardCallbackButton("Да","btnYes"),
                                                    // Second column
                                                    new Telegram.Bot.Types.InlineKeyboardButtons.InlineKeyboardCallbackButton("Нет","btnNo"),
                                                   },
                                       });
                        break;
                    }
                default:
                    {
                        sbAnswer.AppendLine("Чтобы начать, напишите мне /start.");
                        break;
                    }
            }
            return sbAnswer.ToString();
        }
    }
}
