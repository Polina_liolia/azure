﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyBot
{
    public enum BotEvents
    {
        Start,
        StartAccepted,
        StartRejected,
        Scissors,
        Stone,
        Paper,
        Undefined
    }
}
