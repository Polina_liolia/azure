﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.Azure;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using Microsoft.WindowsAzure.Storage.File;
using Microsoft.WindowsAzure.Storage.Table;
using Microsoft.WindowsAzure.Storage.Queue;

namespace BlobConsoleApplication
{
    class Program
    {
        static void Main(string[] args)
        {
            // Parse the connection string and return a reference to the storage account.
            CloudStorageAccount storageAccount = CloudStorageAccount.Parse(
            CloudConfigurationManager.GetSetting("StorageConnectionString"));

            //Создание клиента службы BLOB-объектов
            //тип клиента определяет, к какой части хранилища я подключаюсь.
            CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();

            #region Create container
            //Создание контейнера
            CloudBlobContainer container = blobClient.GetContainerReference("mycontainer");
            container.CreateIfNotExists();
            container.SetPermissions(new BlobContainerPermissions { PublicAccess = BlobContainerPublicAccessType.Blob });
            #endregion


            #region  Sending
            //retrieve refference to a previously created container
            container = blobClient.GetContainerReference("mycontainer");
            //retrieve refference to a blob named "myblob"
            CloudBlockBlob blockBlob = container.GetBlockBlobReference("myblob");
            //create or roverride the "myblob" withcontents from a local file:
            using (var fileStream = System.IO.File.OpenRead(@"d:\MyData\azure_links.txt"))
            {
                blockBlob.UploadFromStream(fileStream);
            }
            #endregion

            #region Scanning
            container = blobClient.GetContainerReference("mycontainer");
            foreach (IListBlobItem item in container.ListBlobs(null, false))
            {
                if (item.GetType() == typeof(CloudBlockBlob))
                {
                    CloudBlockBlob blob = (CloudBlockBlob)item;

                    Console.WriteLine("Block blob of length {0}: {1}", blob.Properties.Length, blob.Uri);

                }
                else if (item.GetType() == typeof(CloudPageBlob))
                {
                    CloudPageBlob pageBlob = (CloudPageBlob)item;

                    Console.WriteLine("Page blob of length {0}: {1}", pageBlob.Properties.Length, pageBlob.Uri);

                }
                else if (item.GetType() == typeof(CloudBlobDirectory))
                {
                    CloudBlobDirectory directory = (CloudBlobDirectory)item;

                    Console.WriteLine("Directory: {0}", directory.Uri);
                }
            }
            #endregion

        }
    }
}
