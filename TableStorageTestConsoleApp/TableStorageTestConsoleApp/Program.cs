﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.Azure;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Table;
using Microsoft.WindowsAzure.Storage.Auth;


namespace TableStorageTestConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            // Parse the connection string and return a reference to the storage account.
            CloudStorageAccount storageAccount = CloudStorageAccount.Parse(
                           CloudConfigurationManager.GetSetting("StorageConnectionString"));
            // Create the table client.
            CloudTableClient tableClient = storageAccount.CreateCloudTableClient();
            // Retrieve a reference to the table.
            CloudTable table = tableClient.GetTableReference("people");
            // Create the table if it doesn't exist.
            table.CreateIfNotExists();

            #region Insert entity

            CustomerEntity customer1 = new CustomerEntity("Harp", "Walter");
            customer1.Email = "Walter@contoso.com";
            customer1.PhoneNumber = "425-555-0101";

            TableOperation insertOperation = TableOperation.Insert(customer1);

            table.Execute(insertOperation);

            #endregion

            #region retrieve and update

            // Create a retrieve operation that takes a customer entity.
            //TableOperation retrieveOperation = TableOperation.Retrieve<CustomerEntity>("Smith", "Ben");
            TableOperation retrieveOperation = TableOperation.Retrieve<CustomerEntity>("Smith", "Ben");
            // Execute the operation.
            //TableResult retrievedResult = table.Execute(retrieveOperation);
            TableResult retrievedResult = table.Execute(retrieveOperation);
            // Assign the result to a CustomerEntity object.
            CustomerEntity updateEntity = (CustomerEntity)retrievedResult.Result;

            if (updateEntity != null)
            {
                // Change the phone number.
                updateEntity.PhoneNumber = "425-555-1234";

                // Create the InsertOrReplace TableOperation.
                TableOperation insertOrReplaceOperation = TableOperation.InsertOrReplace(updateEntity);

                // Execute the operation.
                table.Execute(insertOrReplaceOperation);

                Console.WriteLine("Entity was updated.");
            }

            else
                Console.WriteLine("Entity could not be retrieved.");

            #endregion

            #region select email only

            // Define the query, and select only the Email property.
            TableQuery<DynamicTableEntity> projectionQuery = new TableQuery<DynamicTableEntity>().Select(new string[] { "Email" });
            // Define an entity resolver to work with the entity after retrieval.
            EntityResolver<string> resolver = (pk, rk, ts, props, etag) => props.ContainsKey("Email") ? props["Email"].StringValue : null;
            foreach (string projectedEmail in table.ExecuteQuery(projectionQuery, resolver, null, null))
            {
                Console.WriteLine(projectedEmail);
            }

            #endregion

            #region Delete table

            // Create the CloudTable that represents the "people" table.
            //CloudTable table = tableClient.GetTableReference("people");
            table = tableClient.GetTableReference("people");
            // Delete the table it if exists.
            table.DeleteIfExists();

            #endregion


            Console.WriteLine("Press any key...");
            Console.ReadKey();
        }
    }

    //Table ==> Key, Value
    //создаю структуру Value части
    public class CustomerEntity : TableEntity
    {
        /*
         нужно создать правило для заполнения partition key и row key
         они вдвоем - составной ключ, который должен однозначно характеризовать строку с value
        */
        public CustomerEntity(string lastName, string firstName)
        {
            this.PartitionKey = lastName;
            this.RowKey = firstName;
        }
        public string Email { set; get; }
        public string PhoneNumber { get; set; }
        /*
         коммент для JAVA
         1) Такой же конструктор по умолчанию
         2) Field:
         private string email
         Property:
         public string setEmail(string val);
         public string getEmail();

         */

       

    }
}
