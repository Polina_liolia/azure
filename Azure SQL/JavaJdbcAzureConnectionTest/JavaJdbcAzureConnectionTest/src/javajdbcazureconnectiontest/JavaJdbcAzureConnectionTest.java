package javajdbcazureconnectiontest;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

public class JavaJdbcAzureConnectionTest {


    public static void main(String[] args) {
         Connection conn = null;
        try {
            //String dbURL = "jdbc:sqlserver://azure2700.database.windows.net:1433;database=azuredb2700;user=artimed@azure2700;password={Azure$13};encrypt=true;trustServerCertificate=false;hostNameInCertificate=*.database.windows.net;loginTimeout=30;";
            String dbURL = "????";
            conn = DriverManager.getConnection(dbURL);
            if (conn != null) {
                DatabaseMetaData dm = (DatabaseMetaData) conn.getMetaData();
                System.out.println("Driver name: " + dm.getDriverName());
                System.out.println("Driver version: " + dm.getDriverVersion());
                System.out.println("Product name: " + dm.getDatabaseProductName());
                System.out.println("Product version: " + dm.getDatabaseProductVersion());
                Statement stmt = conn.createStatement();
                try {
                    //stmt.setQueryTimeout(iTimeout);
                    ResultSet rs = stmt.executeQuery("select id,[Description] from [dbo].[orders]");
                    try {
                        while (rs.next()) {
                            String sResult = rs.getString("Description");
                            int nResult = rs.getInt("ID");
                            
                            System.out.print(sResult);
                            System.out.print(" ");
                            System.out.println(nResult);
                        }
                    } finally {
                        try {
                            rs.close();
                        } catch (Exception ignore) {
                        }
                    }
                } finally {
                    try {
                        stmt.close();
                    } catch (Exception ignore) {
                    }
                }
                //
            
            }
        } catch (SQLException ex) {
            Logger.getLogger(JavaJdbcAzureConnectionTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
}
