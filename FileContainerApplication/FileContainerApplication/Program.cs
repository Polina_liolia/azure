﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.Azure;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using Microsoft.WindowsAzure.Storage.File;
using Microsoft.WindowsAzure.Storage.Table;
using Microsoft.WindowsAzure.Storage.Queue;

namespace FileContainerApplication
{
    class Program
    {
        static void Main(string[] args)
        {
            // Parse the connection string and return a reference to the storage account.
            CloudStorageAccount storageAccount = CloudStorageAccount.Parse(
            CloudConfigurationManager.GetSetting("StorageConnectionString"));

            //Создание клиента службы файлов
            //тип клиента определяет, к какой части хранилища я подключаюсь.
            CloudFileClient fileClient = storageAccount.CreateCloudFileClient();
            CloudFileShare share = fileClient.GetShareReference("logs");

            //enshure, that the share exists
            if(share.Exists()) //второй запуск приложения
            {
                Console.WriteLine("Share exists");
                CloudFileDirectory sampleDir = share.GetRootDirectoryReference();
                if (sampleDir.Exists())
                {
                    CloudFile file = sampleDir.GetFileReference("sample-file.txt");
                    if (file.Exists())
                    {
                        Console.WriteLine(file.DownloadTextAsync().Result);
                    }
                }
            }
            else
            {
                //первый запуск приложения
                Console.WriteLine("Share does not exists");
                share.CreateIfNotExists();
                // Create a new file in the root directory.
                CloudFile sourceFile = share.GetRootDirectoryReference().GetFileReference("sample-file.txt");
                sourceFile.UploadText("A sample file in the root directory.");
                // Get a reference to the blob to which the file will be copied.
                CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();
                CloudBlobContainer container = blobClient.GetContainerReference("sample-container");
                container.CreateIfNotExists();
                CloudBlockBlob destBlob = container.GetBlockBlobReference("sample-blob.txt");
                // Create a SAS for the file that's valid for 24 hours.
                // Note that when you are copying a file to a blob, or a blob to a file, you must use a SAS
                // to authenticate access to the source object, even if you are copying within the same
                // storage account.
                string fileSas = sourceFile.GetSharedAccessSignature(new SharedAccessFilePolicy()
                {
                    // Only read permissions are required for the source file.
                    Permissions = SharedAccessFilePermissions.Read,
                    SharedAccessExpiryTime = DateTime.UtcNow.AddHours(24)
                });
                // Construct the URI to the source file, including the SAS token.
                Uri fileSasUri = new Uri(sourceFile.StorageUri.PrimaryUri.ToString() + fileSas);
                // Copy the file to the blob.
                destBlob.StartCopy(fileSasUri);
                // Write the contents of the file to the console window.
                Console.WriteLine("Source file contents: {0}", sourceFile.DownloadText());
                Console.WriteLine("Destination blob contents: {0}", destBlob.DownloadText());
            }

            
            Console.WriteLine("Press any key...");
            Console.ReadKey();

        }
    }
}
